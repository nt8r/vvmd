/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2022, akrosi8
 *                2022, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// Constants used for initializing Verizon's vvm3
// From: https://cs.android.com/android/platform/superproject/+/master:packages/apps/Dialer/java/com/android/voicemail/impl/protocol/Vvm3Subscriber.java;drc=ba93c4fe5016341d06be1ce47410885522e09a58;l=110
#define SPG_VZW_MDN_PARAM "VZW_MDN"
#define SPG_VZW_SERVICE_PARAM "VZW_SERVICE"
#define SPG_DEVICE_MODEL_PARAM "DEVICE_MODEL"
#define SPG_LANGUAGE_PARAM "SPG_LANGUAGE_PARAM"
#define SPG_APP_TOKEN_PARAM "APP_TOKEN"

#define SPG_VZW_SERVICE_BASIC "BVVM"
#define SPG_DEVICE_MODEL_ANDROID "DROID_4G"
#define SPG_APP_TOKEN "q8e3t5u2o1"
#define SPG_LANGUAGE_EN "ENGLISH"

#define VVM3_DEVICE_MODEL "Pixel 5"

//You need to have the . before the domain you are validating!
#define KNOWN_VVM3_HOSTS ".vzw.com"

void vvm3_activation (const char *stripped_number,
                      const char *activate_url);
char *vvm3_xml_find_spgUrl (GString *data);
char *vvm3_html_find_buttonUrl (GString *data);
int validate_vvm3_host (const char *spgUrl);
